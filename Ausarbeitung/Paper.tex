\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper, left=3cm, right=3cm, top=3cm, bottom=3cm]{geometry}
\usepackage{graphicx}
\usepackage{color}
\graphicspath{ {./images/} }
\usepackage[ngerman]{babel}
\usepackage{float}
\usepackage{xcolor}
\usepackage{soul}

\definecolor{Light}{gray}{.90}
\sethlcolor{Light}

\newcommand{\shellcmd}[1]{\texttt{\hl{\$ #1}}}

\title{Git - Versionsverwaltung}
\author{Salah Atallah, Charlotte Baumgarten, Lingzhi Pähler}
\date{März 2022}



\begin{document}
\maketitle
\section{Einleitung}
Versionskontrollen sind heute nicht mehr aus der IT-Branche raus zu denken. Zum Einsatz kommt die Versionsverwaltung insbesondere zur Verwaltung von dem Quellcode, doch auch bei Büroanwendungen wird kaum mehr auf diese verzichtet. \\
In der Textverarbeitung und Softwareentwicklung stellt sich manchmal nach stundenlanger Arbeit heraus, dass die getätigten Änderungen negative Auswirkungen auf das Projekt haben. Mit einem Versionskontrollsystem (kurz VCS; Englisch für Version Control System) lässt sich so etwas leicht beheben. Dieses macht es möglich, Änderungen an einer Datei oder an einer Gruppe von Dateien zu speichern und diese zu protokollieren. Es ist auch später immer noch möglich, auf eine vorherige Version zurückzugreifen, da jede archivierte Version mit einem Zeitstempel und einer Benutzererkennung gespeichert wird. Zusätzlich kann man viele Varianten seiner Datei gleichzeitig abspeichern.\\
Gegenstand dieser Ausarbeitung ist die Funktionsweise solcher verteilten Versionsverwaltungen.\\
Git ist ein solches verteiltes Versionskontrollsystem und bietet seinen Benutzern Werkzeuge zur Verwaltung der Dateien. Es werden die verschiedenen Typen von Versionsverwaltungen und deren Unterschiede erläutert. Außerdem wird mehr darauf eingegangen, was Git ist und wofür dieses genauer benutzt wird. Danach startet die Ausarbeitung mit den Grundlagen von Git und erklärt einige seiner Werkzeuge. Um weiter in die Git-Materie einzutreten, werden die Themen Branches und Feature Branching erläutert und deren Vorteile in der Versionsverwaltung gezeigt. \\
Das Ziel dieser Ausarbeitung ist es, den Lesern die Vorzüge eines Versionskontrollsystems vorzulegen und den Einstieg in die Grundlagen von Git zu vereinfachen. Git bietet sehr viele Möglichkeiten, auch wenn wir nur einige von ihnen erläutern können. Die Hauptquelle unserer Recherche ist das Git-Book, welches noch weitere Aspekte von Git und dessen verteilte Versionskontrolle erläutert\cite{chacon_straub_2014}.


\section{Typen von Versionsverwaltung}
In der \textit{Versionsverwaltung} geht es darum, verschiedene Versionen von Dateien zu speichern und Änderungen über die Zeit zu protokollieren. Dies ist sehr hilfreich, wenn es darum geht, in einer Gruppe an einem Projekt zu arbeiten. Falls etwas schiefläuft, hat man die Möglichkeit einzelne Dateien oder ein ganzes Projekt in eine vorherige Version zurückzusetzen.
Weiterhin lässt es sich einfach nachzuvollziehen, was für bzw. wer Änderungen vorgenommen hat. Auch das simultane Arbeiten zwischen Entwicklern ist ein wichtiger Aspekt der Versionsverwaltung. Besonders in der Softwareentwicklung, bei der Verwaltung von Quelltext, kommt Versionsverwaltung zum Einsatz. Aber auch als Grafik- oder Webdesigner, bei Büroanwendungen und Content-Management-Systemen ist die Anwendung einer Versionsverwaltung eine gute Idee. Dabei gibt es verschiedene Arten der Versionsverwaltung.
\subsection{Lokale Versionskontrolle}
Die einfachste Art ist die \textit{lokale Versionsverwaltung}. Dabei speichert der Benutzer seine Dateien in einfachen Verzeichnissen, welche im Idealfall mit Zeitstempeln markiert sind. Demnach liegt das Archiv ausschließlich auf der Festplatte. Ein populäres Versionsverwaltungssystem Revision Control System (RCS) speichert
für jede Veränderung ein Patch auf der Festplatte.
Um nun eine bestimmte Version aufzurufen, werden alle Patches bis zur gewünschten Version
rekonstruiert. Es gibt einige Nachteile bei der lokalen Versionsverwaltung. 
Zum einen ermöglicht dies keine Teamarbeit.
Zum anderen, falls etwas schiefläuft und man keine Backups gemacht hat, verliert man alle Daten, da alles nur lokal gespeichert ist. 
\subsection{Zentrale Versionskontrolle}
Um das Problem mit der Teamarbeit zu lösen, gibt es die \textit{zentrale Versionsverwaltung}. 
Dabei wird das Archiv auf einem zentralen Server verwaltet und die Clients haben die Möglichkeit, auf das
Archiv zuzugreifen. Das Abholen der Dateien heißt „Auschecken“. Den Entwicklern ist es damit möglich kontrollierter zu arbeiten, da man alle anderen Dateien auch im Überblick hat und auch in der Lage ist Teile des Projekts zu \textit{locken}, sodass andere Entwickler nichts verändern können. Dennoch besteht weiterhin eine hohe Fehleranfälligkeit, da während eines Systemausfalls die Zusammenarbeit und das Abspeichern der Daten nicht funktioniert. Doch der größte Nachteil ist, dass es ausschließlich möglich ist Online zu arbeiten, d. h. man muss immer mit dem Internet verbunden sein, um am Projekt zu arbeiten. 
\subsection{Verteilte Versionskontrolle}
Die \textit{verteilte Versionsverwaltung} kombiniert die Vorteile der lokalen und zentralen Versionskontrolle.
Bei dieser erhält jeder Anwender eine Kopie (einen sogenannten \textit{clone}), des gesamten Archiv. Somit hat jeder ein Backup. Falls
der Server also beschädigt wird, kann ein beliebiges Backup verwendet werden, um den Server wiederherzustellen.
Wie bei der lokalen Versionsverwaltung kann auch offline gearbeitet werden und zu einem späteren Zeitpunkt
die neuste Version auf den Server geladen werden. Weiterhin ist es möglich, in einer Gruppe simultan an einem Projekt
zu arbeiten.

\section{Git}
\subsection{Was ist Git}
Git ist das mit Abstand beliebteste und am weitesten verbreitete Versionskontrollsystem weltweit \cite{GGR}. Das liegt nicht nur an seiner herausragenden Leistung, Sicherheit und Flexibilität, sondern vor allem an seiner verteilten Architektur. Durch das verteilte Versionskontrollsystem ermöglicht Git den Entwicklern an einem Projekt gemeinsam zu arbeiten. Dabei ist es egal, wo sich die Entwickler jeweils aufhalten oder wann jeder Entwickler weiter am Projekt arbeiten möchte. Dabei ist nur wichtig, dass Änderungen protokolliert und nachvollzogen werden können.\\
Git betrachtet die Daten anders als andere Versionsverwaltungen. Manche Systeme betrachten die Informationen, die sie verwalten sollen, als eine Reihe von Daten, an denen im Laufe der Zeit Änderungen vorgenommen werden. Git betrachtet seine Daten eher als eine Reihe von Schnappschüssen eines Mini-Dateisystems. Jedes Mal, wenn man den Status des Projekts commitet (also den gegenwärtigen Status des Projekts als Git-Version speichern) macht Git ein Abbild von allen Dateien und speichert dieses als Verweis. Um dies effizient zu tun, kopiert Git unveränderte Dateien nicht, sondern legt eine Verknüpfung zu der vorherigen Version an. Somit betrachtet Git die Daten eher als Stapel von Schnappschüssen (siehe Abbildung \ref{fig:Schnappschuessen}).  \\
\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.5]{Ausarbeitung/Graphs/StapelVonSchnappschüssen.png}
    \caption{Stapel der ''Schnappschüsse'' in Git \cite{chacon_straub_2014}}
    \label{fig:Schnappschuessen}
\end{figure}


\subsection{Geschichte von Git}
Git ist ein freies Softwareprojekt, welches seine erste Version 2005 veröffentlichte. Das ausgereifte Open-Source-Projekt wurde ursprünglich von Linus Torvalds, dem Entwickler des Linux Betriebssystem-Kernels, entwickelt. Linus Torvalds benötigte ein Werkzeug zur Versionsverwaltung von Quellcode für sein Projekt des Linux Betriebssystem-Kernel. Dabei war wichtig, dass das Werkzeug schnell ist, aber auch die Fähigkeit besitzt große Projekte effektiv und einfach zu verwalten. Außerdem sollten parallel tausende von Entwicklungszweigen entstehen, damit man auf diese wieder zurückgreifen könnte. Obwohl sich Git seit 2005 immer weiter entwickelt, sind die ursprünglichen Ziele dieses Werkzeuges gleich geblieben. Versionsverwaltungen gab es schon vor Git, doch Linus Torvalds war mit den Vorgängern von Git nicht zufrieden und entwickelte sein eigenes Open-Source-Projekt.

\subsection{Git gegenüber anderen Versionsverwaltungen}
Obwohl Git 2005 entstanden ist, wird dieses Versionsverwaltungssystem gegenüber anderen Versionsverwaltungen häufig bevorzugt. Das liegt vor allem an den Vorteilen, welche Git mit sich bringt. Sollte man zum Beispiel ein Projekt starten und nicht dauerhaft auf eine Netzwerkverbindung angewiesen sein wollen, um an dem Projekt zu arbeiten, so ist Git eine gute Wahl. Das gilt außerdem, wenn man Wert auf eine sehr schnelle Übertragung der Änderung legt oder im Falle eines Ausfalls oder Verlusts eines Hauptverzeichnisses  abgesichert sein möchte. 
Sollte man aber seine gesamte Arbeit an einem zentralen Ort bündeln oder auch die Strukturen leerer Verzeichnisse vollständig aufzeichnen wollen, so sind andere Versionsverwaltungen eher geeignet, da Git Verzeichnisse verwirft, die keinen Inhalt besitzen. Git arbeitet allerdings auch nicht sehr gut mit Bilddateien oder mit Videoformaten.  

\section{Git Grundlagen}
\subsection{Repository}
In Git wird das \textit{Repository} auf dem Rechner jedes Nutzers lokal gespeichert, mit kompletter Historie und voller Versionsverfolgung, unabhängig von der Netzwerkverbindung oder einem zentralen Server.\\
Es gibt zwei Methoden, um ein Git Repository anzulegen:
\begin{itemize}
    \item Ein lokales Verzeichnis, welches derzeit nicht unter einer Versionskontrolle steht, kann in ein Git-Repository umgewandelt werden, indem man zunächst in einem Terminal in das gewünschten Verzeichnis navigiert. Nachdem man sich im richtigen Verzeichnis befindet, muss der folgende Befehl eingegeben werden: \shellcmd{git init}. Mit diesem Befehl wird ein Unterverzeichnis \verb|.git| erzeugt, das alle benötigten Git-Repository Dateien enthält.
    \item Ein existierendes Repository kann kopiert werden, damit man an einem Projekt mitarbeiten kann. Dies kann mit dem Befehl \shellcmd{git clone [url]} durchgeführt werden. Dieser Befehl ist wichtig, denn damit lädt Git fast alle Daten, die der Server hat, auf den Computer.
\end{itemize}

\subsection{Zustände von Dateien}
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.075]{Ausarbeitung/Graphs/dateistatus.png}
    \caption{Der Lebenszyklus des Status einer Datei}
    \label{fig:dataeistatus}
\end{figure}
Eine Datei im Git-Repository kann zwei unterschiedliche Zustände haben. Eine Datei kann entweder \textit{Untracked} oder \textit{Tracked} sein. \\
Dateien, welche sich im Untracked-Zustand befinden, sind Dateien, von denen Git noch nichts weiß. Das könnten zum Beispiel neu hinzugefügte Dateien im Arbeitsverzeichnis sein. Mit dem Befehl \shellcmd{git add <file name>} wird die Datei zum Repository, in die Staging-Area, hinzugefügt. Dateien, die sich in der Staging-Area befinden, sind bereit um in Git gespeichert zu werden.
Jetzt weiß Git, dass diese Datei existiert und sie hat den Status \textit{Staged}, welcher ein Typ von Tracked ist. \\
Um nun den Status \textit{Unmodified} zu erhalten, muss die Datei \textit{commitet} werden. Wenn eine Datei commitet wird, speichert Git einen Schnappschuss der Datei in ihrem aktuellen Zustand. Dies geschieht mit dem Befehl \shellcmd{git commit}. Beim Eingeben des Befehls wird ein Editor geöffnet, um eine Commit-Nachricht zu schreiben. Eine Alternative dazu wäre der Befehl \shellcmd{git commit -m \dq<Commit-Nachricht>\dq}. Jetzt ist die Datei im Zustand Unmodified, welche ein Typ von Tracked ist.\\
Eine Datei, die bereits commitet wurde, wird den Status \textit{Modified} bekommen, wenn sie geändert wird. Das bedeutet, dass es eine Änderung seit dem letzten Commit gibt und damit noch nicht richtig abgespeichert worden ist. Um das Repository auf den neuesten Stand zu bringen, müssen die Änderungen erneut hinzugefügt und commitet werden.


\subsection{Commits und Commit Historie}
Der Befehl \shellcmd{git commit} speichert alle Dateien, die im Staged-Status sind, permanent in die Commit Historie. Modified- und Untracked-Dateien werden hierbei nicht übergeben. Neben einer Commit-Nachricht hat man noch die Möglichkeit eine Beschreibung hinzuzufügen, um die Änderung detaillierter zu beschreiben: \shellcmd{Git commit -m \dq<Commit-Nachricht>\dq \: \dq<Commit-Beschreibung>\dq}. \\
Um sich einen Einblick über die bisherigen Commits machen zu wollen, benötigt man den Befehl \shellcmd{git log}. Die Auflistung beginnt mit den neusten Commits und zeigt chronologisch alle anderen Commits. Pro Eintrag werden Prüfnummer des Commits, der Name und die E-Mail-Adresse des Autors, das Erstellungsdatum, die Commit-Nachricht und ggf. die Commit-Beschreibung angegeben. Der Befehl \shellcmd{git log} lässt sich aber noch weiter modifizieren. Falls viele Commits von einem Teammitglied hinzugefügt wurden, kann man mit \shellcmd{git log -p -<Anzahl letzten Eintraege>} die Unterschiede einsehen,  die beim jeweiligen Commit eingefügt worden sind. Diese werden dann unter jedem Eintrag angezeigt. Möchte man unter jedem Eintrag die jeweils geänderten Dateien und die Anzahl hinzugefügter bzw. entfernter Zeilen einsehen, so benutzt man \shellcmd{git log --stat}. Wenn man alle Commits auf einmal einsehen möchte, kann der Befehl \shellcmd{git log --pretty=oneline} nützlich sein. Dieser zeigt alle Commits und deren Commit-Nachricht an. Sollte man zum Beispiel nach einem Commit vergessen haben eine Datei hinzuzufügen, so kann man ohne Probleme die Datei im Anschluss noch hinzufügen und mit \shellcmd{git commit --amend} commiten. Dieser Befehl verhindert, dass der Repository Verlauf mit Kleinigkeiten überladen wird, denn der vorherige Commit wird nun durch den neuen Commit ersetzt. Es gibt noch viele weitere Befehle, um die Commit-Historie einzusehen, doch dies würde hier den Rahmen sprengen.

\subsection{Dateien löschen und verschieben}
Natürlich gibt es in Git Möglichkeiten Dateien zu löschen oder Änderungen rückgängig zu machen. 
Wenn man mit \shellcmd{git status} {\"A}nderungen in der Staging-Area findet, die man unstagen möchte, kann man mit \shellcmd{git restore --staged <file>} die entsprechende Datei aus der Staging-Area entfernen. Diese bleibt immer noch verändert, falls sie es davor war.\\
Wenn man jedoch eine Änderung an einer Datei rückgängig machen möchte, welche unstaged ist, kann man mit \shellcmd{git restore <file>} die Datei auf den Stand des letzten Commits bringen.\\
Um eine Datei aus dem Arbeitsverzeichnis zu entfernen, gibt es den Befehl \shellcmd{git rm <file>}. Dadurch wird die Datei aus der Staging-Area und des Dateisystems entfernt und anschließend muss commitet werden. Weiterhin ist es auch möglich Dateien umzubenennen oder zu verschieben mithilfe des Befehls \shellcmd{git mv <Dateipfad\textbackslash Name> <Dateipfad\textbackslash Name>}. Dabei wird die erste Datei aus dem Verzeichnis gelöscht und dann ins entsprechende Verzeichnis hinzugefügt.  

\subsection{Arbeiten mit Remotes}
Um in einem Team zu arbeiten, gibt es die \textit{Remote-Repositorys}. Diese werden in den meisten Fällen im Internet angeboten, wobei GitHub eines der meistgenutzten Hosts ist. Ein weiterer Host wäre GitLab, welcher von der RWTH-Aachen unterstützt wird. Alternativ kann das Remote-Repository auch auf dem lokalen Rechner eingerichtet werden. Um die Arbeit mit den Teammitgliedern zu teilen, werden die Daten \textit{gepusht} bzw. \textit{gepullt}.
Um die neue Version der Datei auf den Server hochzuladen, wird der Befehl  \shellcmd{git push} verwendet. Um dagegen die neuen Daten, des Remote-Projekts herunterzuladen, gibt es  \shellcmd{git fetch}. Jedoch werden die Daten nicht mit dem lokalen Repository zusammengefügt. Mit \shellcmd{git pull} werden die neuen Daten dann heruntergeladen und direkt mit dem lokalen Repository sinnvoll integriert. Damit wird das lokale Repository auf den aktuellsten Stand gebracht. Bevor man anfängt an seinem Projekt weiterzuarbeiten, sollte man also immer die aktuellsten Daten pullen.\\
Mit den üblichen Befehlen, kann man auch mehrere Remote-Repositorys einrichten, sowie entfernen. 
%(Ab hier kann weggelassen werden  für Platz) Anzeigen lassen sie sich mit \shellcmd{git remote}. Dabei wird das Repository, welches man geklont hat, als \textit{origin} angezeigt. Mit \shellcmd{git remote -v} lassen sich auch die URLs anzeigen, die jeweils verwendet wurden.\\
%Natürlich hat man bisher nur das Repository, welches geklont wurde. Um weitere Remotes hinzuzufügen, nutzt man einfach den Befehl \shellcmd{git remote add <shortname> <url>}. Mit \shellcmd{git fetch <remote>} erhält man die Informationen zum jeweiligen Remote, welche noch nicht im eigenen Repository sind.\\
%Wenn man in seinem eigenen Projekt gearbeitet hat und das Projekt mit dem Team teilen möchte, kann man seinen \textit{Main-Branch} auf den zugehörigen Remote pushen. Mit \shellcmd{git push origin master} werden alle Commits auf den origin Remote-Repository übertragen. Außerdem ist es möglich bereits angelegte Remotes mit \shellcmd{git remote rename <remote> <neuerName>} umzubenennen und mit \shellcmd{git remote remove <Name>} zu löschen.

\subsection{Taggen}
Um ein Git Projekt transparenter für das Team zu gestalten, kann man, wie in vielen anderen VCS,
auch in Git bestimmte Punkte markieren bzw. taggen. Meist wird der Release einer fertigen Version, 
wie z. B. v1.0,v1.1 des Projekts getaggt. Es kann auch genutzt werden, um einen historischen Punkt
zu markieren, welcher dann dazu da ist, um Daten wiederherzustellen. \\
Hier wird der Commit 3 mit `v1.0' und der Commit 5 mit `v1.1' gettagt (siehe Abb.\ref{fig:tagsbeispiel}).\\
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.7]{Ausarbeitung/Graphs/Tags.png}
    \caption{Beispiel von Tags}
    \label{fig:tagsbeispiel}
\end{figure}
$\\$
Um ein \textit{lightweight} Tag zu erstellen, gibt es den Befehl \shellcmd{git tag <tagname>}, 
welcher einen Tag für den aktuellen Commit erstellt. Dieser hat dann nur Informationen über die Prüfsumme des jeweiligen Commits.
Wenn man mehr Informationen haben möchte, kann man \textit{annotated} Tags verwenden.  Dieser Tag wird als vollständiges Objekt im Repository gespeichert und dieses enthält alle Informationen über den Tag, also bspw. den Tagger-Namen, die E-Mail-Adresse und das Datum.\\
In der Regel benutzt man die annotated Tags, um mehr Informationen zu enthalten. Wenn man jedoch einen temporären Tag erstellen möchte oder aus irgendeinem Grund diese Informationen nicht speichern möchte, bieten sich auch die lightweight Tags an. Des Weiteren gehen lightweight Tags, im Gegensatz zu annotated Tags, beim Rebasing verloren.\\
Annotated Tags haben, ähnlich wie auch Commits, eine Tag-Nachricht. Der einfachste Weg um einen annotated Tag zu erstellen ist mit dem Befehl 
\shellcmd{git tag -a <tagname> -m <\dq Nachricht\dq>}.
Mit \shellcmd{git tag} lassen sich dann alle Tags anzeigen, wobei man bspw. mit \shellcmd{git tag -l "v1.*"} nach allen Tags, die mit 'v1.' anfangen, suchen kann.\\
Bisher ist es nur möglich den aktuellen Commit zu taggen. Möchte man einen älteren Commit taggen, kann man dies mit \shellcmd{git tag -a <tagname> <Pruefsumme des Commits>} tun, indem am Ende des Befehls die Prüfsumme des Commits angegeben wird, wobei es ausreicht den Anfang der Prüfsumme anzugeben.\\
Um ein Tag zu löschen kann man einfach den Befehl \shellcmd{git tag -d <tagname>} verwenden. 
Dabei muss man darauf achten, dass auch der Tag vom Remote entfernt wird, insofern man den Tag schon auf das Remote gepusht hat. Ein einfacher Weg ist es den Befehl \shellcmd{git push origin --delete <tagname>} zu verwenden. Damit der Tag erst einmal auf dem Remote ist, muss der Tag mit \shellcmd{git push origin <tagname>} hinzugefügt werden. Das kann sehr umständlich sein, wenn es viele Tags gibt, die man pushen möchte. 
Dann kann man mit \shellcmd{git push --tags} alle Tags gleichzeitig pushen.
%\subsection{Beisect}

\section{Branches}
\subsection{Was sind Branches}
\textit{Branching} bedeutet, dass die Entwickler von der Hauptlinie des Projekts abzweigen und so an einzelnen kleineren Aufgaben arbeiten können. Damit verhindert man Fehler in der Hauptlinie, welche auch \textit{Master-} oder \textit{Main-Branch} genannt wird \cite{gruesso_2021}. Um Branches besser zu verdeutlichen, wird zuerst der Main-Branch beschrieben. Der Main-Branch in Git ist der Standard Branch, wo das Hauptprojekt und deren Quellcode gespeichert wird. Möchte nun ein Entwickler an einer bestimmten Stelle den Code verändern und etwas ausprobieren, so sollte er dies nicht im Main-Branch machen. Um die Arbeitsaufteilung unter den Entwicklern aufzuteilen, kann jeder Entwickler seinen separaten Branch erstellen und an diesem arbeiten. Sollten diese abgeschlossen und abgesichert sein, so kann man die separaten Teile zusammenführen (auch \textit{Merging} genannt).

\subsection{Branching und Merging}
Ein neuer Branch entsteht durch den Befehl \shellcmd{git branch }. Dieser Branch zeigt nun auf denselben Commit wie der vom Main-Branch. Damit ist der Branch nun erstellt, aber man befindet sich noch nicht in diesem Branch. Dies lässt sich in Abbildung \ref{fig:2branches} erkennen. \textit{HEAD} zeigt noch immer auf den Main-Branch, obwohl der neue Branch erstellt worden ist. Um zwischen den Branches zu wechseln, benötigt man \shellcmd{git checkout}. Um HEAD jetzt auf testing zu verschieben wird der Befehl \shellcmd{git checkout testing} benutzt. Also der Befehl \shellcmd{git checkout} wird genutzt um zwischen Branches zu navigieren.
$\\$
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{Ausarbeitung/Graphs/2branches.png}
    \caption{Zwei Branches, die auf den gleichen Commit zeigen}
    \label{fig:2branches}
\end{figure}
$\\$
Wenn wir beim Testen ein Problem finden, dann können wir einen neuen Branch (zum Beispiel den Branch namens hotfix wie in Abbildung \ref{fig:2branchesmerge}) vom Main-Branch machen, um dieses Problem zu lösen, ohne den Main-Branch zu beeinflussen. Nachdem das Problem behoben worden ist, kann man wieder in den Main-Branch wechseln, aber ohne alles neu implementieren zu müssen. Dies geschieht, indem man die beiden Branches zusammen mergt. Dafür braucht man den Befehl \shellcmd{git merge}. Dabei ist wichtig, dass HEAD noch auf den Main-Branch zeigt. Nach erfolgreicher Zusammenführung kann der hotfix Branch gelöscht werden, da nun beide Branches auf denselben Commit zeigen. Dies macht man mit dem Befehl \shellcmd{git branch -d}. Diese Art von Merging heißt \textit{Fast-Forwarding}, da beide Branches auf den gleichen Pfad zeigen. \\
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.57]{Ausarbeitung/Graphs/FFMerge.png}
    \caption{Hotfix und Main-Branch vor und nach merging}
    \label{fig:2branchesmerge}
\end{figure}
$\\$
Es gibt noch eine andere Art von Merging, nämlich den \textit{Three-Way-Merge}. Diesen benutzt Git, wenn man zwei Branches, welche nicht auf den gleichen Pfad zeigen, mergen möchte. Dies wird in Abbildung \ref{fig:3wm} veranschaulicht. Hier werden die beiden letzten Commits von beiden Branches mit dem nächstgelegenen gemeinsamen Vorgänger verglichen und der Three-Way-Merge wird durchgeführt (die drei rot markierten Commit werden verglichen).\\ 
Manchmal kann Git die Branches nicht automatisch mergen und gibt einen \textit{Merge-Konflikt} aus.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.55]{Ausarbeitung/Graphs/3WM.png}
    \caption{Beispiel eines Three-Way-Merges}
    \label{fig:3wm}
\end{figure}
\\
\subsubsection{Merge-Konflikte}
Merging und Konflikte gehört bei der Arbeit mit Git dazu. Konflikte entstehen meistens, wenn zwei oder mehrere Personen dieselben Zeilen in einer Datei geändert oder gelöscht haben. In diesen Fällen kann Git nicht entscheiden, welcher Vorgang nur richtig ist. Dieser Konflikt betrifft aber nur den Entwickler, der den Merge durchführen möchte. Der Rest der Entwickler bemerkt von dem Konflikt nichts. Git bezeichnet die betreffende Datei als „in Konflikt stehend“ und bricht den Merge Vorgang ab. Dann ist es die Aufgabe des Entwicklers, diesen Konflikt zu lösen.\\
Es gibt zwei Arten von Merge-Konflikten. Dieser kann zu Beginn oder während des Merge-Vorgangs passieren.\\
Wenn ein Merge-Konflikt am Start des Merge-Vorgangs passiert, dann liegt das häufig daran, dass Git Änderungen im Arbeitsverzeichnis des aktuellen Projekts erkennt. Dies liegt nicht an Konflikten mit anderen Entwicklern, sondern an Konflikten mit ausstehenden lokalen Änderungen. Dies kann schnell mit einem Commit-Befehl behandelt werden. \\
Ein Fehler während eines Merge-Vorgangs ist ein Konflikt mit dem Code eines anderen Entwicklers. Git wird hier versuchen die Dateien zu mergen, doch gibt die Möglichkeit den Fehler manuell zu lösen. Git bietet für beide Arten von Konflikte hilfreiche Tools, um die Konflikte zu lösen oder ihnen aus dem Weg zu gehen.
%Manchmal kann Git die Branches nicht automatisch mergen und gibt einen \textit{Merge-Konflikt} aus. Das kann passieren, wenn derselbe Teil in beiden Branches von unterschiedlichen Entwicklern geändert wurde. Hier ist Git nicht in der Lage, die Branches ohne Interaktion der Entwickler zu mergen.\\

%\begin{figure}[H]
    %\centering
    %\includegraphics[scale=0.55]{Ausarbeitung/Graphs/3WM.png}
    %\caption{Beispiel eines Three-Way-Merges}
    %\label{fig:3wm}
%\end{figure}
%\subsection{Branch-Management}
%Das obere Beispiel zeigt ein Beispiel mit wenigen Branches. Wenn das Projekt aber größer wird, ist es wichtig, die Werkzeuge zur Verwaltung der Branches zu kennen. Der Befehl \shellcmd{git branch} wurde bereits erklärt, aber bisher wurde er nur zur Erstellung oder Löschung von Branches verwendet. Wenn man diesen Befehl ohne Argumente ausführt, wird einem eine Liste der aktuellen Branches aufgelistet. Einer der Branches wird ein Sternchen (*) haben, da dieser Branch gerade ausgecheckt wird. Der Flag \verb|-v| macht das Gleiche, zeigt aber auch die letzte Commit-Nachricht an.

\subsection{Rebasing}

\textit{Rebasing} ist eine andere Methode, um Branches zu integrieren. Bei dem Beispiel bei Abbildung \ref{fig:rebase} hatte man den Branch und Main-Branch einfach zusammen gefügt und so führt Git einen Three-Way-Merge zwischen beide Branches ein. Bei diesem Beispiel kann man auch die Rebasing-Methode anwenden. Beim Rebasing können die Änderungen, die im Beispiel Branch vorgenommen wurden, an die Spitze des Main-Branches gebracht werden. Dies passiert, indem man den Branch auscheckt und auf die Main-Branch mit dem Befehl \shellcmd{git rebase main} rebaset. Jetzt befindet sich Main-Branch und den Branch im selben Pfad (siehe Abb. \ref{fig:rebase}), aber Main zeigt immer noch auf einen älteren Commit. Also führt man einfach den Fast-Forward-Merge durch und somit zeigen beide Branches auf denselben Commit.\\
Rebasing kann ein nützliches Werkzeug sein, um die Commit-Historie eines Projekts zu verbessern. Es kann aber auch ein gefährliches Werkzeug sein, weil die Commit-Historie verändert wird und sich dadurch die Art und Weise, wie das Projekt entstanden ist, ändert.\\
Um das Repository nicht zu zerstören wird empfohlen, einen Rebase niemals in einen gepushten Branch einzuführen \cite{chacon_straub_2014}.
%Es wird auch empfohlen, niemals einen Rebase auf einem gepushten Branch einzuführen\cite{chacon_straub_2014}, weil das Repository schnell und einfach kappt gehen kann.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{Ausarbeitung/Graphs/rebase.png}
    \caption{Beispiel für Rebasing}
    \label{fig:rebase}
\end{figure}
\section{Feature Branching}

\subsection{Branching-Strategien}
Branching-Strategien sind eine teaminterne Vereinbarung darüber, wie und wann Branches in Git oder anderen Versionskontrollsystemen erstellt und wieder zusammengefügt werden. Wie man Branches verwendet ist hier jedem Team selbst überlassen, doch diese sollte man auf jeden Fall vor Beginn des Projekts festlegen. In Git ist es üblich, mehrere Kopien des Repositorys und damit mehrere maßgebliche Datenquellen zu erstellen, wobei die Entwickler in der Regel eine zentrale oder primäre Kopie benennen. Dann wird parallel an den jeweiligen Repositorys gearbeitet und man teilt im Anschluss seine Arbeit.\\
Eine andere Strategie wäre es, häufige Commits von den Teammitgliedern zu erwarten. Dadurch kann regelmäßiger getestet und verhindert werden, dass am Ende doch nicht alles zusammen passt.
Dann gibt es noch die Feature-Branching-Strategie, wobei einzelne Features vom Rest des Codebestands getrennt werden. Auch bei Branching-Strategien gibt es viele verschiedene Arten, nur wichtig dabei ist, diese zu Beginn festzulegen.

\subsection{Was ist Feature Branching}
\textit{Feature Branching} ist eine Branching-Strategie, welche bei jeder geplanten Änderung in der Haupt-Codebasis einen neuen Branch (welche auch Feature genannt werden) erstellt und dort entwickelt werden kann, ohne die Basis zu demolieren. Das Ziel dieses Musters ist es immer einen stabilen Code zu haben, den man theoretisch ausliefern könnte. \\
Bei Git wird das Feature Branching genutzt, um die Entwicklung neuer Features zu isolieren und getrennt zu entwickeln. Erst nach einer Review werden die Änderungen tatsächlich übernommen. Die Feature-Entwicklung findet aber niemals im Main-Branch statt, um zu verhindern, dass die Haupt-Codebasis zerstört wird. Dadurch wird der Main-Branch niemals beschädigten Code erhalten.\\
Dies ist allerdings nur eine Strategie um das Entwickeln eines Projekts zu vereinfachen und keine Pflicht um Git zu benutzen.\\
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{Ausarbeitung/Graphs/Feature-Branching.png}
    \caption{Abzweigung einzelner Feature vom Main-Branch}
    \label{fig:featurebranches}
\end{figure}

\subsection{Feature Branch Workflow in Git}
Die Idee hinter dem Feature-Branch-Workflow ist, dass die gesamte Projekt-Entwicklung in verschiedenen Branches passiert und nicht im Main-Branch. Dieses Entkapselung erleichtert mehreren Entwicklern die Arbeit an einem bestimmten Feature, ohne die Haupt-Codebasis zu stören. Durch das Absegnen des Branches von anderen Kollegen ist eine Demolierung des Main-Branches fast unmöglich. Man ist aber nicht verpflichtet an seinem Feature alleine zu arbeiten. Das Teilen eines Features mit anderen Entwicklern ermöglicht schnelles Feedback und vereinfacht den Merge-Vorgang. 

\subsection{Wie funktioniert Feature Branching}
Im folgenden Beispiel zeigen wir ein Szenario, in dem ein Feature-Branch-Workflow zum Einsatz kommt\cite{BT}.
$\\$
\underline{Anna beginnt ein neues Feature:}\\
Bevor Anna eine neue Entwicklung am Projekt beginnt, braucht sie einen isolierten Branch. Sie erstellt mit dem branch-Befehl einen neuen Branch namens \glqq annas-feature\grqq{} und wechselt in diesen Branch. Auf diesem Feature kann Anna nun Änderungen wie gewohnt bearbeiten, stagen und so viele Commmits erstellen wie sie braucht.
$\\$
\underline{Anna geht zur Mittagspause:}\\
Anna fügt ihrem Feature einige Commits hinzu. Bevor sie aber zum Mittagessen geht, pusht sie ihren Feature zum zentralen Repository. Sollte Anna zum Beispiel mit anderen Entwicklern zusammengearbeitet haben, so hätten diese auch Zugriff auf ihre Commits. Außerdem ist das hochladen ihres Branches ein praktisches Backup.
$\\$
\underline{Anna beendet ihr Feature:}\\
Nach dem Mittagessen stellt Anna ihr Feature fertig. Nun ist es wichtig, dass Anna sich die Änderungen des zentralen Repositorys pullt und testet, ob ihr Feature auch kombinierbar mit der aktuellsten Main-Branch-Version ist. Wenn nicht, muss sie weiter an ihrem Feature arbeiten. Wenn nun alles funktioniert informiert Anna ihr Team, dass sie fertig und bereit ist in den Main-Branch zu mergen. Nun wartet sie auf eine Rückmeldung ihrer Kollegen.
$\\$
\underline{Klaus schaut sich Annas Feature an:}\\
Klaus erhält die Nachricht, dass Annas Feature bereit ist zu mergen. Doch Klaus möchte einige Änderungen an dem Feature vornehmen bevor das Feature in das zentrale Repository hinzugefügt wird. Anna und Klaus tauschen sich aus und Anna ändert ihr Feature. Wenn Klaus wollte, könnte er sich Anna Feature auch klonen und an diesem selber arbeiten.
$\\$
\underline{Anna fügt ihr Feature hinzu:}\\
Sobald Annas Kollegen ihr Feature abgesegnet haben kann Anna ihr Feature mergen. Git mergt die beiden Branches mit einem Three-Way-Merge und erstellt einen neuen Merge-Commit um die Branches zu vereinigen.
$\\$
\underline{Anna löscht ihr Feature:}\\
Anna hat ihr Feature erfolgreich gemergt und braucht ihr Feature nun nicht mehr. Sie entscheidet sich dafür den Branch \glqq annas-feature\grqq{} zu löschen. Das kann sie mit \shellcmd{git branch -d annas-feature} machen. Dieser Befehl funktioniert aber nur, wenn Anna dieses Feature erfolgreich gemergt hat. Sollte sie das vorher nicht gemacht haben, so gibt Git ihr eine Fehlermeldung, da so der Verlust des Zugriffs auf den Branch verhindert wird. Nun testen Anna nochmal zur Sicherheit den Main-Branch und kann nach erfolgreichem Bestehen des Tests mit einer neuen Entwicklung starten.
$\\$
\underline{Alternativ:}\\
Sollte Anna mit ihrer Entwicklung nicht zufrieden sein, ein neues Projekt anfangen oder einfach neu starten wollen so kann sie, wenn sie möchte, den Branch löschen und einen neues Feature beginnen. Hier hat sie den Branch aber noch nicht gemergt und würde eine Fehlermeldung bekommen. Dies verhindet sie indem sie den Befehl \shellcmd{git branch -D annas-feature} benutzt. Hier wird Annas Feature ohne Fehlermeldung gelöscht.


\section{Zusammenfassung}
Git wurde in dieser Ausarbeitung erklärt.
Nach der Erläuterung, was eine Versionskontrolle ist und welche Unterschiede es dort gibt, ging der Text weiter auf das verteilte Versionskontrollsystem Git ein. Dieses arbeitet anders als andere Systeme und speichert die Versionen der Dateien in Schnappschüssen ab.\\ Obwohl Git 2005 gegründet worden ist, ist dieses heute eines der bekanntesten Versionsverwaltungssysteme weltweit. Nachdem Git und andere Versionsverwaltungssysteme verglichen wurden, folgten die Grundlagen von Git. Zunächst wurden die Begriffe wie Repository, Commit, Staged und weiter Zustandsarten einer Datei in Git erläutert. Diese konnten teilweise mit Terminalbefehlen unterlegt werden. Nach den Commits, welche essentiell für Git sind, konnte man lernen, wie man Dateien in Git löscht und hinzufügt. Remotes und Tags bildeten den Schluss der Git-Basis. Danach lenkten wir das Interesse des Lesers auf Branches und auf Feature Branches. Nahezu jedes VSC unterstützt einer Form des Branching, welche den Entwicklern ermöglicht der Hauptlinie der Entwicklung eines Projekts abzuweichen und in den Branches zu arbeiten, ohne diese Hauptlinie zu zerstören. Im Gegensatz zu einigen anderen Versionsverwaltungssystemen ermutigt Git den Benutzern Branches und Merging zu verwenden. Mit den Themen Branch-Management und Rebasing wurde der Abschnitt mit Branches beendet. Doch Branches sind wichtig für die Feature-Branching-Strategie. Feature Branching ist eine Strategie, um Entwickler das Arbeiten mit einem größeren Projekt leichter und unkomplizierter zu machen. Auch bei diesem Muster gibt es einen besonderen Workflow und eine Funktionsweise. In der Ausarbeitung folgt die Funktionsweise anhand eines einfachen Beispiels. Um das Thema abzurunden, wurden noch Branching-Strategie erläutert, welche nützlich für die Entwicklung des jeweiligen Projekts sein könnten. Außerdem wird hier nochmal deutlich gemacht, dass beim Branching und Merging immer mal Konflikte passieren können und es wird gezeigt, wie man diese vermeiden und beheben kann. Dennoch ist das Thema Git und Versionsverwaltung ein so großes Thema, dass in dieser Ausarbeitung einige Gebiete nur angerissen wurden.

\pagebreak

\bibliographystyle{alpha}
\bibliography{sample}
\nocite{*}


%Scott Chancon, Ben Staub. Pro Git - Everything you need to know about git. Apress. 2014 (Zweite Edition)\\
%Marco Prillwitz. Die unterschiedlichen Varianten des Branching und Merging. HOOD Block Group GmbH. 16.09.2014\\

%Was ist Git Quelle:
%https://www.atlassian.com/de/git/tutorials/atlassian-git-cheatsheet

\end{document}